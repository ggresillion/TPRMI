package iad.rmi.chat;


import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.util.Scanner;

public class ChatServer {

    private Registry registry;

    public ChatServer() {
        try {
            this.registry = LocateRegistry.createRegistry(1099);
        } catch (RemoteException e) {
            e.printStackTrace();
        }
    }

    public static void main(String[] args) {
        try {
            ChatServer server = new ChatServer();
            Scanner scanner = new Scanner(System.in);
            System.out.println("Please enter a name for the lobby : ");
            String name = scanner.next();
            System.out.println("Please enter a description : ");
            String desc = scanner.next();
            ChatConferenceImpl conf = new ChatConferenceImpl(name, desc);
            server.registry.rebind(name, conf);
            System.out.println("Lobby " + name + " created !");
            System.out.println("Available commands : list, stop");
            while (true) {
                String next = scanner.next();
                if (next.equals("list")) {
                    System.out.println("Online users :");
                    for (ChatParticipant participant : conf.participants) {
                        System.out.println(participant.getName());
                    }
                    System.out.println("");
                } if (next.equals("stop")){
                    return;
                }
            }
        }
        catch (RemoteException e){
            e.printStackTrace();
            return;
        }
    }
}
