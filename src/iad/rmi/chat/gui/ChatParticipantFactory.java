package iad.rmi.chat.gui;

import iad.rmi.chat.ChatParticipant;
import iad.rmi.chat.ChatParticipantImpl;

import java.rmi.RemoteException;

public class ChatParticipantFactory {

    private static ChatParticipant participant;

    public static ChatParticipant getParticipant() {
        return participant;
    }

    public static ChatParticipant createParticipant(String name) throws RemoteException {
        participant = new ChatParticipantImpl(name);
        return participant;
    }
}
