package iad.rmi.chat.gui;

import iad.rmi.chat.ChatMessage;
import iad.rmi.chat.ChatParticipant;
import iad.rmi.chat.ChatParticipantListener;
import javafx.application.Platform;
import javafx.fxml.FXML;
import javafx.scene.control.ListView;
import javafx.scene.control.TextArea;

import java.rmi.RemoteException;

public class ChatController implements ChatParticipantListener{

    private ChatParticipant participant;

    @FXML
    private ListView<String> messages;

    @FXML
    private TextArea message;

    @FXML
    public void initialize() {
        participant = ChatParticipantFactory.getParticipant();
        try {
            participant.addListener(this);
        } catch (RemoteException e) {
            e.printStackTrace();
        }
    }

    @FXML
    private void onSendBtn() throws RemoteException {
        if(message.getText()==""){
            return;
        }
        participant.send(message.getText());
        message.clear();
    }

    @Override
    public void onNewMessage() {
        //Workaround : throws exception if not in current thread
        Platform.runLater(() -> {
        ChatMessage msg = null;
        try {
            msg = ChatParticipantFactory.getParticipant().next();
        } catch (RemoteException e) {
            e.printStackTrace();
        }
        if (msg != null) {
            messages.getItems().add(msg.getEmitter() + " : " + msg.getContent());
        }
    });
    }
}
