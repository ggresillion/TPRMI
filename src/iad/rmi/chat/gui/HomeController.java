package iad.rmi.chat.gui;

import iad.rmi.chat.ChatConference;
import iad.rmi.chat.ChatParticipant;
import iad.rmi.chat.ChatParticipantImpl;
import iad.rmi.chat.gui.ChatParticipantFactory;
import javafx.application.Platform;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ListView;
import javafx.scene.control.TextArea;
import javafx.stage.Stage;

import java.io.IOException;
import java.rmi.NotBoundException;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;

public class HomeController {

    private ObservableList<String> lobbyList;
    private Registry registry;
    static private ChatParticipant participant;

    @FXML
    private ListView<String> lobbies;

    @FXML
    private TextArea username;

    @FXML
    private void onOkBtn(ActionEvent event) throws IOException, NotBoundException {
        if(username.getText()==null || username.getText().equals("")){
            Alert alert = new Alert(Alert.AlertType.WARNING);
            alert.setTitle("No username");
            alert.setHeaderText("Please give a username !");
            alert.showAndWait();
            return;
        }
        if(lobbies.getSelectionModel().getSelectedItem()==null){
            Alert alert = new Alert(Alert.AlertType.WARNING);
            alert.setTitle("No lobby selected");
            alert.setHeaderText("Please select a lobby !");
            alert.showAndWait();
            return;
        }
        participant = ChatParticipantFactory.createParticipant(username.getText());
        ChatConference conf = (ChatConference) registry.lookup(lobbies.getSelectionModel().getSelectedItem());
        participant.join(conf);
        Parent chat = FXMLLoader.load(getClass().getResource("chat.fxml"));
        Scene chatScene = new Scene(chat);
        Stage chatStage = (Stage) ((Node) event.getSource()).getScene().getWindow();
        chatStage.setScene(chatScene);

    }

    @FXML
    public void initialize() {
        try {
            registry = LocateRegistry.getRegistry("localhost", 1099);
            if (lobbies != null) {
                lobbyList = lobbies.getItems();
                lobbyList.addAll(registry.list());
                lobbies.setItems(lobbyList);
            }
        } catch (RemoteException e) {
            e.printStackTrace();
            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.setTitle("Connexion error");
            alert.setHeaderText("Could not establish connection !");
            alert.showAndWait();
            Platform.exit();
        }
    }
}
