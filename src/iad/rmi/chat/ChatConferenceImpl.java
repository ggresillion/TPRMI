package iad.rmi.chat;

import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;
import java.util.ArrayList;

public class ChatConferenceImpl extends UnicastRemoteObject implements ChatConference {


    private  String name;
    private  String description;
    private Boolean isStarted;
    public ArrayList<ChatParticipant> participants;

    public ChatConferenceImpl(String name, String description) throws RemoteException {
        super();
        this.participants = new ArrayList<ChatParticipant>();
        this.name = name;
        this.description = description;
    }

    @Override
    public String getName() throws RemoteException {
        return this.name;
    }

    @Override
    public String getDescription() throws RemoteException {
        return this.description;
    }

    @Override
    public boolean isStarted() throws RemoteException {
        return this.isStarted;
    }

    @Override
    public void addParticipant(ChatParticipant p) throws RemoteException {
        this.broadcast(new ChatMessage("Server", p.getName() + " has joined the conference"));
        this.participants.add(p);
    }

    @Override
    public void removeParticipant(ChatParticipant p) throws RemoteException {
        this.broadcast(new ChatMessage("Server", p.getName() + " has left the conference"));
        this.participants.remove(p);
    }

    @Override
    public String[] participants() throws RemoteException {
        String[] participantsNames = new String[participants.size()];
        int count = 0;
        for(ChatParticipant participant: participants){
            participantsNames[count] = participant.getName();
            count++;
        }
        return participantsNames;
    }

    @Override
    public void broadcast(ChatMessage message) {
        for(ChatParticipant participant: participants){
            try {
                participant.process(message);
            } catch (RemoteException e) {
                participants.remove(participant);
            }
        }
    }

    @Override
    public void start() throws RemoteException {
        this.isStarted = true;
    }

    @Override
    public void stop() throws RemoteException {
        this.isStarted = false;
    }
}
