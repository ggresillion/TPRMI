package iad.rmi.chat;

import java.io.IOException;
import java.rmi.NotBoundException;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.util.Scanner;

public class ChatClient {

    public static void main(String[] args) throws IOException, NotBoundException {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Please enter a user name : ");
        String name = scanner.next();
        ChatParticipant participant = new ChatParticipantImpl(name);
        Registry registry = LocateRegistry.getRegistry("localhost", 1099);
        System.out.println("List of available lobbies :");
        int i = 0;
        for(String lobby: registry.list()) {
            System.out.println(i++ + " - " + lobby);
        }
        System.out.println("Which lobby do you want to join ?");
        int num = Integer.parseInt(scanner.next());
        ChatConference conf = (ChatConference) registry.lookup(registry.list()[num]);
        participant.join(conf);
        System.out.println("Joined lobby : " + registry.list()[num].toString());
        ChatClientConsole console = new ChatClientConsole(conf, participant);
        console.run();
    }
}