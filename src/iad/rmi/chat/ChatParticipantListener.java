package iad.rmi.chat;

public interface ChatParticipantListener {
    public void onNewMessage();
}
