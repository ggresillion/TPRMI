package iad.rmi.chat;

import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.Queue;

public class ChatParticipantImpl extends UnicastRemoteObject implements ChatParticipant {

    private ChatConference conference;
    private String name;
    private Queue<ChatMessage> messages;
    private boolean isConnected;
    private ArrayList<ChatParticipantListener> listeners;

    public ChatParticipantImpl(String name) throws RemoteException {
        super();
        this.name = name;
        this.messages = new LinkedList<>();
        this.listeners = new ArrayList<>();
    }

    @Override
    public boolean join(ChatConference conference) throws RemoteException {
        this.conference = conference;
        conference.addParticipant(this);
        isConnected = true;
        return isConnected;
    }

    @Override
    public void leave(ChatConference conference) throws RemoteException {
        conference.removeParticipant(this);
        this.conference = null;
    }

    @Override
    public void send(String txt) throws RemoteException {
        this.conference.broadcast(new ChatMessage(this.getName(),txt));
    }

    @Override
    public void process(ChatMessage msg) throws RemoteException {
        this.messages.add(msg);
        this.fireNewMessage();
    }

    private void fireNewMessage() {
        for(ChatParticipantListener listener : listeners){
            listener.onNewMessage();
        }
    }

    @Override
    public void addListener(ChatParticipantListener listener) throws RemoteException {
        if(listener!=null) {
            this.listeners.add(listener);
        }
    }

    @Override
    public void removeListener(ChatParticipantListener listener) throws RemoteException {
        this.listeners.remove(listener);
    }

    @Override
    public boolean isConnected() throws RemoteException {
        return isConnected;
    }

    @Override
    public String getName() throws RemoteException {
        return this.name;
    }

    @Override
    public ChatMessage next() throws RemoteException {
        return this.messages.poll();
    }
}
