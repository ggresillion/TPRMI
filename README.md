# TP RMI

Repo du tp RMI de Jessy Declerck et Guillaume Grésillion.
L'interface utilise les libraries JavaFX.

Pour utiliser notre application, vous pouvez soit build les jars correspondant au serveur et au client, soit utiliser les fat jars fournies à la racine du projet. Pour ce faire, deux commandes suffisent:

  java -jar Server.jar
  
  java -jar ClientCLI.jar
